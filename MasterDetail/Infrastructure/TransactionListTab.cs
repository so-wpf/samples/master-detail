﻿namespace MasterDetail.Infrastructure
{
    public class TransactionListTab : ITabVm
    {
        public string Title { get; } = "Transactions";
    }
}

﻿namespace MasterDetail.Models
{
    using System.Collections.Generic;

    public class ValidCategories : List<Category>
    {
        public ValidCategories()
        {
            //Since we don't have any backend to work with, we're going to prefill this stuff here
            Add(new Category("General", "A general catch-all transaction category"));
            Add(new Category("Food", "Food related transaction"));
            Add(new Category("Holiday", "Holiday related transactions"));
            Add(new Category("Shopping", "Shopping related transaction"));
            Add(new Category("Travel", "Travel related transaction"));
        }
    }
}

﻿namespace MasterDetail.Models
{
    using System;

    public class Category : IEquatable<Category>
    {
        public Category(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public string Name { get; }

        public string Description { get; }

        public bool Equals(Category other)
        {
            return !(other is null) && (ReferenceEquals(this, other) ||
                    string.Equals(Name, other.Name) &&
                    string.Equals(Description, other.Description));
        }

        public override bool Equals(object obj)
        {
            return !(obj is null) &&
                   (ReferenceEquals(this, obj) || obj.GetType() == GetType() && Equals((Category) obj));
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ (Description != null ? Description.GetHashCode() : 0);
            }
        }

        public static bool operator ==(Category left, Category right) => Equals(left, right);

        public static bool operator !=(Category left, Category right) => !Equals(left, right);
    }
}
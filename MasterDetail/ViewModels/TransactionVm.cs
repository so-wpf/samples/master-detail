﻿namespace MasterDetail.ViewModels
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using JetBrains.Annotations;
    using Models;

    public class TransactionVm : INotifyPropertyChanged
    {
        string _recipient;
        double _amount;

        public string Recipient
        {
            get => _recipient;
            set
            {
                if (value == _recipient) return;
                _recipient = value;
                OnPropertyChanged();
            }
        }

        public double Amount
        {
            get => _amount;
            set
            {
                if (value.Equals(_amount)) return;
                _amount = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Category> Categories { get; } = new ObservableCollection<Category>();

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}

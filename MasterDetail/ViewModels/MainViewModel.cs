﻿namespace MasterDetail.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using Infrastructure;
    using JetBrains.Annotations;
    using Models;

    public class MainViewModel : INotifyPropertyChanged
    {
        TransactionVm _activeTransaction;

        public MainViewModel()
        {
            //this stuff would normally be coming from backend but since we're in a sample..
            //Also i'm just randomly adding categories, normally you would have built this up 
            //from db lookup
            Transactions.Add(new TransactionVm { Recipient = "McDonalds", Amount = 5.43, Categories = { ValidCategories[1] } });
            Transactions.Add(new TransactionVm { Recipient = "Arcade Gaming", Amount = 15.6, Categories = { ValidCategories[0] } });
            Transactions.Add(new TransactionVm { Recipient = "Magic Shroom Travel Agency", Amount = 50.95, Categories = { ValidCategories[3] } });
        }

        public ObservableCollection<TransactionVm> Transactions { get; } = new ObservableCollection<TransactionVm>();

        public TransactionVm ActiveTransaction
        {
            get => _activeTransaction;
            set
            {
                if (Equals(value, _activeTransaction)) return;
                _activeTransaction = value;
                OnPropertyChanged();
            }
        }

        //Tabs are static so we have no need to declare this as an OC
        public IEnumerable<ITabVm> Tabs { get; } = new List<ITabVm> { new TransactionListTab(), new ActiveTransactionEditTab() };

        public ValidCategories ValidCategories { get; } = new ValidCategories();

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
